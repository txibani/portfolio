///////////////////////////////////////////////////////////////////
// SUBMIT REGISTRATION FORM
///////////////////////////////////////////////////////////////////

// - GLOBAL VARIABLES
 var formComment = $('#newComment');
 var menu = $('#navBar'),
     menuHeight = menu.height();
 var flag = true,
     flag2 = true;

// - FUNCTIONS

function validateForm() {
    'use strict';
    formComment.validate({
        rules: {
            secondName: {
                required: true,
                minlength: 2
            },
            email: {
                required: true,
                email: true
            },
            telephone: {
                required: false,
                number: true
            }
        }
    });
}

function submitForm() {
    'use strict';
    var btnText = $('#btnRegister');
    formComment.on('submit', function(e) {
        e.preventDefault();
        var _this = $(this);

        var data = {
            firstName: _this.find('#firstName').val(),
            secondName: _this.find('#secondName').val(),
            email: _this.find('#email').val(),
            phone: _this.find('#telephone').val(),
            comment: _this.find('#comments').val()
        };

        if (_this.valid()) {
            $.ajax({
                type: 'POST',
                url: 'http://maitegracia.co.uk/php/proces.php', //process to mail
                data: data,
                contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
                success: function() {
                    formComment.find('input').val('');
                    formComment.find('textarea').val('');
                    btnText
                      .html('Sent! Thank you :)')
                      .css('background', '#A7FFEB');
                },
                error: function() {
                    console.log('failure');
                }
            });
        }
    });
}

// - DISPLAY ANIMATION WHEN SCROLLING TO SECTION

function progressBar() {
  'use strict';
  $(window).scroll(function() {
      var windowScroll = $(window).scrollTop();
      var skillsBreack = $('.skills').offset().top,
          skillsBreackPoint = skillsBreack - menuHeight*2;
      var studiesBreakPoint = $('.experience').offset().top - menuHeight*2;

      if (windowScroll > skillsBreackPoint) {
        if (flag) {
          $(".meter > span").each(function () {
            flag = false;
            var widthSpan = $(this).data('width');
            $(this)
              .html(widthSpan)
              .data("origWidth", widthSpan)
              .width(0)
              .animate({
                width: $(this).data("origWidth")
              }, 1200);
          });
        }
      }
      if (windowScroll > studiesBreakPoint) {
         if (flag2) {
           flag2 = false;
           var slideL = $('.slideLeft');
           var slideR = $('.slideRight');
           slideL
              .css('opacity', '1')
              .addClass('animated slideInLeft');
           slideR
               .css('opacity', '1')
               .addClass('animated slideInRight');
         }
      }
    });
}


// - MENU CHANGE STYLE ON SCROLL

function menuChange() {
  'use strict';
      var triangleHeight = $('.triangleHeight').height(),
          removeBorder = menu.find('.navbar-collapse'),
          windowHeight = $(window).height(),
          valueChange = windowHeight + triangleHeight - menuHeight*7;
    $(window).scroll(function() {
      if ($(window).scrollTop() >= valueChange) {
        menu.addClass('changeBackground');
        removeBorder.css('border-bottom', 'none');
      } else {
        menu.removeClass('changeBackground');
        removeBorder.css('border-bottom', 'solid 1px #607D8B');
      }
    });
}


// - SCROLL TO SPECIFED SECTION

function btnMove() {
  'use strict';
  var clicked = $('#menu a');
  clicked.on('click', function() {
      var _this = $(this);
      var currentClick = _this.attr('href').slice(1);
      var target = $('.sectionModule[data-text="'+ currentClick +'"]');
      var targetTop = $('.parallax[data-text="'+ currentClick +'"]');
        if (currentClick ==='top') {
          $('html, body').animate({
            scrollTop: targetTop.offset().top
          });
        } else {
          $('html, body').animate({
            scrollTop: target.offset().top - menuHeight/2
          });
        }
  });
}

// - CLOSE BUTTON MENU RESPONSIVE

function closeBtn() {
  'use strict';
  var btnCl = $('.closeMenu');
  btnCl.click(function() {
    $('#navbar').toggleClass('in', 1000);
  });
}


// - DOCUMENT READY

$(document).ready(function() {

    'use strict';
    validateForm();
    submitForm();
    progressBar();
    if ($(window).width() > 768) {
        menuChange();
    }
    btnMove();

});
